import os

DEBUG = False
USE_X_FORWARDED_HOST = True
SECURE_SSL_REDIRECT = False
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
SESSION_COOKIE_SECURE = True

with open('/run/secrets/django-secret-key', 'r') as f:
    SECRET_KEY = f.read()

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': '/home/docker/mnt/db/db.sqlite3',
    }
}

STATIC_ROOT = os.environ['DJANGO_STATIC_ROOT']
MEDIA_ROOT = os.environ['DJANGO_MEDIA_ROOT']
STATIC_URL = '/static/'
MEDIA_URL = '/media/'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'match-gunicorn': {
            'format': '[%(asctime)s] [%(process)s] [%(levelname)s] %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S +0000'
        }
    },
    'handlers': {
        'requests': {
            'level': 'INFO',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': 'ps_requests.log',
        },
        'security': {
            'level': 'DEBUG',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': 'ps_security.log'
        },
        'docker-logs': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'match-gunicorn'
        }
    },
    'loggers': {
        'backend': {
            'handlers': ['requests'],
            'level': 'INFO',
            'propagate': False,
        },
        'django.security': {
            'handlers': ['security'],
            'level': 'INFO'
        },
        'django': {
            'handlers': ['docker-logs'],
            'level': 'INFO',
        }
    }
}
