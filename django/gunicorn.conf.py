chdir = '/home/docker/backend'
wsgi_app = 'backend.wsgi'
preload_app = True
bind = '0.0.0.0:8000'
workers = 2
# Keep connection to NGINX alive for long
keepalive = 30
# Send accesslog to stdout
accesslog = '-'
# Match format of the master process of Gunicorn
access_log_format = '%(t)s [%(p)s] [INFO] method=%(m)s, url=%(U)s, query=%(q)s, status=%(s)s, duration=%(M)s'
# Send errorlog to stderr
errorlog='-'
# Send Django output to stderr
capture_output = True


# Customize logging timestamp
import datetime
from gunicorn.glogging import Logger


class CustomLogger(Logger):

    def now(self):
        # Match the format of the master process of Gunicorn
        return datetime.datetime.utcnow().strftime('[%Y-%m-%d %H:%M:%S +0000]')

logger_class = CustomLogger