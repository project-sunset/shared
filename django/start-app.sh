#!/bin/bash
# Crash on first error
set -e

# Setup database
cd /home/docker/backend
python3.12 manage.py migrate --no-input
python3.12 manage.py createcachetable
python3.12 manage.py collectstatic --no-input --clear

# Create default admin user if it doesn't exist
cat <<EOF | python3.12 manage.py shell
from django.contrib.auth import get_user_model

User = get_user_model()  # get the currently active user model

User.objects.filter(is_superuser=True).exists() or \
    User.objects.create_superuser('admin', 'admin@projectsunset.nl', 'admin')
EOF

gunicorn --config /home/docker/gunicorn.conf.py
