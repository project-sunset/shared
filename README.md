# Project Sunset Shared Stuff

## Shared pipeline

Needed GitLab settings: Container Registry

Needed directory structure:

```text
project-name
  backend
    backend
      settings
        - acceptance.py # from .docker import *
        - production.py # from .docker import *
    requirements.txt # includes `coverage`
  frontend
    package.json
  tests
    package.json # playwright testing, includes a project called `smoke-test`
  sonar-project.properties
  Dockerfile
  .gitlab-ci.yml
```

The `.gitlab-ci.yml` should look like this

```yaml
variables:
  ACC_WEBSITE: tehila-acceptance.projectsunset.nl
  PRD_WEBSITE: tehila.projectsunset.nl
  PIPELINE_VERSION: &pipeline_version main # or branch name

include:
  - project: 'project-sunset/shared'
    ref: *pipeline_version
    file: 'shared-pipeline.yml'
```

`package.json` of `tests` should have a `"nyc": "nyc report"` script.

Go to SonarCloud and create a project for the project, add the SONAR_TOKEN as a unprotected, masked variable.

The Dockerfile should look like this

```Dockerfile
ARG PIPELINE_VERSION
FROM registry.gitlab.com/project-sunset/shared/django:${PIPELINE_VERSION}
```

## Deployment

Deployment is an image to deploy websites to PythonAnywhere or the acceptance server.

## Testing

Testing is the image that contains Node, Python and other software to run the tests and the coverage.

## Django

Django is the production-ready image to use for deploying a Django application.


### Debugging acceptance

`docker exec -it <project name> /bin/sh`

logfiles:
- `cat /home/docker/code/backend/ps_requests.log`
- `cat /home/docker/code/backend/ps_security.log`


## CI variables

Get host key: `ssh-keyscan {domain}`, the line with `ssh-rsa` in it
Generate key pair: `ssh-keygen -t rsa "{comment}"`

| Variable               | Where to find                                                   |
| ---------------------- | --------------------------------------------------------------- |
| ACC_SERVER_HOST_KEYS   | host key for acceptance server                                  |
| ACC_SERVER_PRIVATE_KEY | Generate key, put private here and public in /home/deploy/.ssh  |
| PRD_SERVER_HOST_KEYS   | host key of production server                                   |
| PRD_SERVER_PRIVATE_KEY | Generate key, put private here and public in /home/deploy/.ssh  |
