# Deployment image

A minimal alpine image to deploy my websites

## Hoplite

Hoplite is a python script that handles the deployment of my Django websites.

### Configuring

Create a `hoplite.config.json` file in the root of the project, here you can store settings.

|     key    | effect |
|------------|--------|
| `vue_base` | Set the Vue base to not be the root path, this should be the same as in your `vite.config.js`, but it should not start or end with a `/`. |
| `cron_jobs` | Create additional cron jobs (aside from database backup), this should be an array, see below for syntax |

Note that for `vue_base` you need to also add this in your Django settings:
```python
STATICFILES_DIRS = [
    (<VUE_BASE>, os.path.join(BASE_DIR, '../frontend/dist/static'))
]
```

#### Cron jobs

Properties:
- name: this should be unique in the project
- cron_schedule: a cron schedule (minute, hour, day, month, weekday)
- django_command: the django management command that should be executed

Hoplite adds a backup job by default:

```json
{
    "name": "backup",
    "cron_schedule": "0 2 * * *",
    "django_command": "b2_backup"
}
```