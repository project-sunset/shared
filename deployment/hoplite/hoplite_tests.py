import unittest

from unittest.mock import patch, mock_open, MagicMock
from hoplite import parser
from nginxconf import NginxConfBuilder

class HopliteTests(unittest.TestCase):

    @patch('hoplite.run')
    @patch('hoplite.os.makedirs')
    @patch('hoplite.open', mock_open(read_data='test'))
    def test_deploy(self, makedirs_patch: MagicMock, run_patch: MagicMock):
        args = parser.parse_args(['deploy', '--url', 'www', '--env', 'acceptance', '--projectname', 'test-project', '--version', 'main'])
        args.func(args)

        run_patch.assert_any_call(['docker', 'pull', 'registry.gitlab.com/project-sunset/test-project:main'], check=True)
        run_patch.assert_any_call('crontab -l | { cat -; echo "0 2 * * * docker exec --workdir /home/docker/backend test-project-acceptance-django-1 python manage.py b2_backup --settings=backend.settings.acceptance >> /home/deploy/test-project-acceptance/cron.log 2>&1 # test-project-acceptance - backup"; } | crontab -', shell=True, check=True)

    @patch('hoplite.run')
    @patch('hoplite.rmtree')
    @patch('hoplite.os.remove')
    def test_delete(self, remove_patch: MagicMock, rmtree_patch: MagicMock, run_patch: MagicMock):
        args = parser.parse_args(['delete', '--env', 'acceptance', '--projectname', 'test-project'])
        args.func(args)

        run_patch.assert_any_call(['docker', 'compose', '--file', '/home/deploy/test-project-acceptance/compose.yml', '--project-name', 'test-project-acceptance', 'down', '--volumes'])
        run_patch.assert_any_call('crontab -l | grep -v "test-project-acceptance" | crontab -', shell=True)

class NginxConfigTests(unittest.TestCase):
    def test_config_ok(self):
        expected = """server {
    listen 443 ssl;
    listen [::]:443 ssl;
    charset utf-8;
    client_max_body_size 75M;
    server_name www.projectsunset.nl;
    set $upstream http://project-sunset-acc:8000;
    resolver 127.0.0.11 valid=30s;
    location /static {
        alias /usr/share/static/project-sunset-acc;
        expires 7d;
    }
    location /media {
        alias /usr/share/media/project-sunset-acc;
    }
    location / {
        proxy_pass $upstream;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Host  $host;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
"""
        actual = NginxConfBuilder().hostname("www.projectsunset.nl").service_name("project-sunset-acc").build()
        self.assertEqual(expected, actual)
    
    def test_config_with_base_ok(self):
        expected = """server {
    listen 443 ssl;
    listen [::]:443 ssl;
    charset utf-8;
    client_max_body_size 75M;
    server_name www.projectsunset.nl;
    set $upstream http://project-sunset-acc:8000;
    resolver 127.0.0.11 valid=30s;
    location /static {
        alias /usr/share/static/project-sunset-acc;
        expires 7d;
    }
    location /media {
        alias /usr/share/media/project-sunset-acc;
    }
    location /app/static {
        alias /usr/share/static/project-sunset-acc/app;
        expires 7d;
    }
    location / {
        proxy_pass $upstream;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Host  $host;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
"""
        actual = NginxConfBuilder().hostname("www.projectsunset.nl").service_name("project-sunset-acc").vue_base("app").build()
        self.assertEqual(expected, actual)

if __name__ == '__main__':
    unittest.main()