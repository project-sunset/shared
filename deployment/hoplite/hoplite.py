import argparse
import json
import os
import secrets
from pathlib import Path
from shutil import rmtree
from string import Template
from subprocess import run

from nginxconf import NginxConfBuilder

class Hoplite:

    _script_dir = os.path.dirname(os.path.abspath(__file__))

    def __init__(self, args):
        self.args = args
        if Path(self._config_file_location).is_file():
            with open(self._config_file_location, 'r') as f:
                self.config = json.load(f)
        else:
            self.config = {}

    def deploy(self):
        print(f'Deploying {self._service_name}, version: {self.args.version}')
        self._pull_new_version()
        self._stop_current_version()
        self._setup_local_env()
        self._update_nginx_config()
        self._reload_nginx()
        self._start_service()
        self._create_cron_jobs()
        print("Deployment finished")
    
    def delete(self):
        print("Deleting deployment")
        self._delete_deployment()
        rmtree(self._app_config_dir)
        os.remove(self._nginx_conf)
        self._reload_nginx()
        self._delete_cron_jobs()
        print("Deployment deleted")

    def _delete_cron_jobs(self):
        run(f'crontab -l | grep -v "{self._service_name}" | crontab -', shell=True)
    
    def _create_cron_jobs(self):
        print("Creating cron jobs")
        self._delete_cron_jobs()
        jobs = [{
            "name": "backup",
            "cron_schedule": "0 2 * * *",
            "django_command": "b2_backup"
        }]
        if 'cron_jobs' in self.config:
            jobs.extend(self.config['cron_jobs'])
        base_command = f"docker exec --workdir /home/docker/backend {self._service_name}-django-1 python manage.py <command> --settings=backend.settings.{self.args.env}"
        for job in jobs:
            redirect_to_log = f">> {self._app_config_dir}/cron.log 2>&1"
            comment_reference = f"# {self._service_name} - {job['name']}"
            cron_line = ' '.join([job["cron_schedule"], base_command.replace("<command>", job["django_command"]), redirect_to_log, comment_reference])
            self._add_to_cron(cron_line)
        print("Cron jobs created")

    def _add_to_cron(self, cron_line: str):
        # Get current cron tab, append the new line, put back into cron
        run(f'crontab -l | {{ cat -; echo "{cron_line}"; }} | crontab -', shell=True, check=True)

    # Copied from Django
    def _get_random_secret_key(self):
        chars = "abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)"
        return "".join(secrets.choice(chars) for _ in range(50))
    
    def _pull_new_version(self):
        run(['docker', 'pull', self._new_image], check=True)
    
    def _stop_current_version(self):
        if Path(self._compose_file).is_file():
            run(['docker', 'compose', 
                '--file', self._compose_file, 
                '--project-name', self._service_name,
                'down'])
    
    def _delete_deployment(self):
        run(['docker', 'compose', 
            '--file', self._compose_file, 
            '--project-name', self._service_name,
            'down', '--volumes'])
    
    def _start_service(self):
        run(['docker', 'compose', 
            '--file', self._compose_file,
            '--project-name', self._service_name,
            'up', '--detach'], check=True)
        
    def _update_nginx_config(self):
        new_nginx_config = NginxConfBuilder().hostname(self.args.url[len('https://'):-1]).service_name(self._service_name)
        if 'vue_base' in self.config:
            new_nginx_config.vue_base(self.config['vue_base'])
        with open(self._nginx_conf, 'w') as nginx_conf:
            nginx_conf.write(new_nginx_config.build())
        
    def _reload_nginx(self):
        run(['/home/bob/sunset-server/nubo/tasks/nginx-reload-config.sh'], check=True)
    
    def _setup_local_env(self):
        os.makedirs(self._app_config_dir, exist_ok=True)
        if not Path(self._django_secret_file).is_file():
            with open(self._django_secret_file, 'w') as f:
                f.write(self._get_random_secret_key())
        with open(self._app_env_settings, 'a') as f:
            pass # just create the file if missing
        with open(self._compose_file, 'w') as f:
            f.write(self._compose_template.substitute({
                'new_image': self._new_image,
                'env': self.args.env,
                'service_name': self._service_name,
                'django_secret_file': self._django_secret_file
            }))
    
    @property
    def _app_env_settings(self):
        return f'{self._app_config_dir}/config_{self.args.env}.py'

    @property
    def _config_file_location(self):
        return f'{self._script_dir}/hoplite.config.json'
    
    @property
    def _new_image(self):
        return f'registry.gitlab.com/project-sunset/{self.args.projectname}:{self.args.version}'

    @property
    def _service_name(self):
        return f'{self.args.projectname}-{self.args.env}'
    
    @property
    def _app_config_dir(self):
        return f'/home/deploy/{self._service_name}'

    @property
    def _compose_file(self):
        return f'{self._app_config_dir}/compose.yml'

    @property
    def _django_secret_file(self):
        return f'{self._app_config_dir}/django-secret-key'

    @property
    def _nginx_conf(self):
        return f'/home/deploy/nginx-conf.d/{self._service_name}.conf'
    
    @property
    def _compose_template(self):
         with open(f'{self._script_dir}/compose.yml', 'r') as f:
            return Template(f.read())
    

class DeployArgs:
    url: str
    env: str
    projectname: str
    version: str

class DeleteArgs:
    projectname: str
    env: str

def deploy(args: DeployArgs):
    Hoplite(args).deploy()

def delete(args: DeleteArgs):
    Hoplite(args).delete()

parser = argparse.ArgumentParser(
    prog='Hoplite',
    description='Deploy manager'
)
subparsers = parser.add_subparsers()

parser_deploy = subparsers.add_parser('deploy')
parser_deploy.add_argument('--url', required=True)
parser_deploy.add_argument('--env', choices=['acceptance', 'production'], required=True)
parser_deploy.add_argument('--projectname', required=True)
parser_deploy.add_argument('--version', required=True)
parser_deploy.set_defaults(func=deploy)

parser_delete = subparsers.add_parser('delete')
parser_delete.add_argument('--projectname', required=True)
parser_delete.add_argument('--env', choices=['acceptance', 'production'], required=True)
parser_delete.set_defaults(func=delete)

if __name__ == '__main__':
    args = parser.parse_args()
    args.func(args)