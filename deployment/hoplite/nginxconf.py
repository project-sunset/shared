
class NginxValidationError(Exception):
    pass

class NginxConfBuilder:
    def __init__(self):
        self.config = ""
        self.current_indent = 0

        self._hostname = None
        self._service_name = None
        self._vue_base = None

    def hostname(self, hostname: str):
        self._hostname = hostname
        return self
    
    def service_name(self, service_name: str):
        self._service_name = service_name
        return self
    
    def vue_base(self, base_path: str):
        if base_path.endswith("/") or base_path.startswith("/"):
            raise NginxValidationError("vue_base needs to not start or end with /")
        self._vue_base = base_path
        return self

    def _add_location(self, location: str, alias: str, expires: str=None):
        self._add_line(f"location {location} {{")
        self._add_line(f"alias {alias};")
        if expires is not None:
            self._add_line(f"expires {expires};")
        self._add_line("}")
    
    def _indent(self):
        return '    '*self.current_indent
    
    def _add_line(self, line: str):
        if line.endswith("}"):
            self.current_indent -= 1
            self.config += f"{self._indent()}{line}\n"
        elif line.endswith("{"):
            self.config += f"{self._indent()}{line}\n"
            self.current_indent += 1
        else:
            self.config += f"{self._indent()}{line}\n"
    
    def _validate(self):
        if self.config != "":
            raise NginxValidationError("Cannot build twice")
        elif self._hostname is None:
            raise NginxValidationError("hostname is not set")
        elif self._service_name is None:
            raise NginxValidationError("service_name is not set")
    
    def build(self):
        self._validate()
        self._add_line("server {")
        self._add_line("listen 443 ssl;")
        self._add_line("listen [::]:443 ssl;")
        self._add_line("charset utf-8;")
        self._add_line("client_max_body_size 75M;")
        self._add_line(f"server_name {self._hostname};")
        self._add_line(f"set $upstream http://{self._service_name}:8000;")
        self._add_line("resolver 127.0.0.11 valid=30s;")
        self._add_location("/static", f"/usr/share/static/{self._service_name}", "7d")
        self._add_location("/media", f"/usr/share/media/{self._service_name}")
        if self._vue_base is not None:
            self._add_location(f"/{self._vue_base}/static", f"/usr/share/static/{self._service_name}/{self._vue_base}", "7d")
        self._add_line("location / {")
        self._add_line("proxy_pass $upstream;")
        self._add_line("proxy_set_header X-Real-IP $remote_addr;")
        self._add_line("proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;")
        self._add_line("proxy_set_header X-Forwarded-Host  $host;")
        self._add_line("proxy_set_header X-Forwarded-Proto $scheme;")
        self._add_line("}")
        self._add_line("}")
        return self.config